package muesli

// Frame of execution of a function in the Muesli VM
type Frame struct {	
	parent    *Frame
	arguments []Value
	results   []Value
	failed    bool
	returned  bool
	position  *Position
}

func NewFrame(parent *Frame, position *Position) *Frame {
	return &Frame{parent, EmptyValueArray(), EmptyValueArray(), false, false, position}
}

