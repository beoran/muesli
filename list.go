package muesli

// ListValue is a Muesli value with a list of other values.
// Lists in Muesli are heterogenous, and can contain other Values of any type.
type ListValue struct {
    RedispatchCallable
	List []Value
}

const ListType   		= TypeValue("List")

func (v ListValue) Type() TypeValue   { return ListType }

func (val ListValue) String() string {
	res := "["
	sep := ""
	for _, elt := range val.List {
        if elt == nil {
            res = res + sep + "nil"
        } else   { 
            res = res + sep + elt.String()
        }
        sep = ", "
	}
	res += "]"
	return res
}

func (from * ListValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *[]Value:
			(*toPtr) = from.List
		case **ListValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from
		default:
			return NewErrorValuef("Cannot convert value %v to %v", from, to)
	}
	return nil
}

func NewListValue(elements ...Value) *ListValue {
	l := &ListValue{ List: elements }
	l.RedispatchCallable = NewRedispatchCallable("List", l)
	return l    
}

func (list *ListValue) Append(elements ...Value) {
	list.List = append(list.List, elements...)
}

func (list *ListValue) AppendList(toAppend ListValue) {
	list.List = append(list.List, toAppend.List...)
}

func (list ListValue) Length() int {
	return len(list.List)
}

func (list *ListValue) Fetch(i int) Value {
	if i >= len(list.List) {
		return NilValue
	}
	return list.List[i]
}

func (list *ListValue) Place(i int, v Value) Value {
	if i >= len(list.List) {
		return NilValue
	}
	list.List[i] = v
	return list.List[i]
}

func (list *ListValue) First() Value {
	return list.Fetch(0)
}

func (list *ListValue) Last() Value {
	return list.Fetch(list.Length()-1)
}


func EmptyListValue() ListValue {
	lv := NewListValue()
	return *lv
}

func EmptyValueArray() []Value {
	return make([]Value, 0)
}

func NewValueArray(elements ...Value) []Value {
	return elements
}
