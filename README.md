# Muesli is a Multi Use Embeddable Scripting Language Implementation.

## Introduction

Muesli is a scripting language implemented in Go, that has a simple command 
chain based TCL or shell like syntax but with rich semantics. Unlike TCL
or shels, all muelsi values are typed, and many types are supported, 
not just string values. Additionaly, if so desired, every symbol used in the 
syntax can be replaced by a key word, leading to a native language programming 
feel.

## Syntax

A Muesli program consists of blocks and chains, separated by newlines or 
periods. A chain consists of one or more commands, linked together by operators.

- A muesli program consists of statements.
- A statement is either a block, or a command chain terminated by a newline, 
  or just an empty line .
- A command chain consists of commands chained together with operators.
  A command chain is evaluated by looking up and calling the the first operator 
  in the chain, which receives the first command and the rest of the chain as 
  arguments. 
- There are tree kinds of operators: substitute operators,
  block operators, and shuffle operators. 
- Substitute operators begin with one of +-*/ and will receive their arguments 
  as already evaluated substitutions.
- Block operators begin with one of |&<> and will recieve their arguments
  as unevaluated blocks.  
- Shuffle operators begin with one of ,; are interpreted as commands and the 
  first operand will be an evaluated block, but the second operand is passed
  as is. 
  Examples: 
  $door , open -> , $door open
  $door , key , drop ->  , (, key $door) drop 
  ls | grep "foo" | sort > fopen "foo" -> | ls grep "foo" 
  is evaluated as: > | (| (ls) (grep "foo")) (sort)) fopen "foo"
- A command may be a direct command, a substitution, or a literal value.
- A direct command consists of a name followed by zero or more parameters.
  A direct command is evaluated as follows: the name is looked up
  in the VM and executed if found and callable, with the given parameters,
  and evaluates to the return value to the callable.
  An error is raised if the name of the direct command is not found.
- A substitution starts with an open parenthesis, followed by a command chain,
  followed by a close parenthesis. a substitution followed by zero or more
  parameters.
  A substitution is evaluated as the return value of the command chain
  inside the parenthesis of the substitution.
- A bracketed list [ elem1 elem2 ... ]  is syntactic sugar for (list
elem1 elem 2)
- A dollar getter $varname is syntactic sugar for (get varname)
- A equals setter =varname value is syntactic sugar for (set varname value)
- Therefore, parenthesis, lists, getters and setters are allowed anywhere,
  also in the beginning of the command with substitution semantics.
- A literal value evaluates to itself.
- A name also evaluates to itself when it is a parameter,
  but if it is at the beginning of a direct command, the name is looked up
  in the VM and called in stead if found, or an error is raised if not found.


Blocks consist of chains grouped together in blocks between braces {}. The 
commands in a block are not executed immediately but stored.

A single chain can be placed in parenthesis (), where the return value of the 
chain will be subsituted.  Lists are formed by placing their contents 
between brackets [].

Every command has a return value, which can be of any type, and not just a 
number like in most shells. Furthermore commands have input and output, which 
default to stdin and stdout, but can be redirected or reconfigured. 

There are three types of commands, namely direct commands, substitutions,
and literal commands. A direct command starts with a name, and is followed by 
any number of parameters. Parameters may be which may be blocks, parenthesis, 
lists, names or literals, or values. An indirect command starts with a 
substitution followed by parameters. A literal command is simply a numeric,
string, boolean or nil literal that evaluates to itself.

The language itself has no control statements, such as if, or for, 
these are implemented as commands in stead.

Muesly supports several kinds of values, namely: 
- Integers
- Floats 
- Strings between "double quotes"
- The booleans !true and !false
- The nil value !nil
- Words, which are simple runs of alphanumercial characters starting with a 
  lower case lettter, such as foo
- Types which are runs of alphanumerical characters starting with an upper 
  case letter, such as Foo.



