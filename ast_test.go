package muesli

import "testing"

func TestAstKind(test *testing.T) {
	var kind AstKind = AstKindError
	errast := NewEmptyAst(AstKindError)
	astnone := NewAstNone()

	if kind != AstKindError {
		test.Errorf("Error is not error: %v", kind)
	}

	if !errast.IsError() {
		test.Errorf("Error is not error: %v", errast)
	}

	if !astnone.IsNone() {
		test.Errorf("None is not none: %v", astnone)
	}
}
