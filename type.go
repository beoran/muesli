package muesli

// TypeValue is used to model the type of a value in the Muesli type system.
// This is simply a string so this can be used as a hash key, however, 
// the name of a type must always begin with an upper case letter.
type TypeValue string
const (
    // TypeType is the type of TypeValue itself.
    TypeType   		= TypeValue("Type")
	// AnyType is not really a type, but a wildcard that matches any type
	AnyType    		= TypeValue("Any")
	// ZeroType in not really a type, but means that the type is missing, 
	// and is conventiently the zero value for TypeValue
	ZeroType   		= TypeValue("")
)



func (val TypeValue) String() string {
	return string(val)
}

func (v TypeValue) Type() TypeValue   { return TypeType }

var _ Value = TypeValue("")


func (from TypeValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *TypeValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from
		default:
			return NewErrorValuef("Cannot convert value %v to %v", from, to)
	}
	return nil
}



