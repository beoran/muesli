package muesli

const WordType   		= TypeValue("Word")
// WordValue are Muesli command words
type WordValue string

func (val WordValue) String() string {
	return string(val)
}

func (v WordValue) Type() TypeValue   { return WordType }

func (from WordValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *WordValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from			
		default:
			return NewErrorValuef("Cannot convert WordValue %v to %v", from, to)
	}
	return nil
}
