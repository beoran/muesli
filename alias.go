package muesli

func (vm *VM) AliasUp(from, to string, level int) Value {
    value := vm.Lookup(to)
    if value == nil {
        return value
    }
    return vm.RegisterUp(from, value, level)
}

func (vm *VM) Alias(from, to string) Value {
    value := vm.Lookup(to)
    if value == nil {
        return value
    }
    return vm.Register(from, value)
}



